package com.brfpfa;

import java.util.Scanner;

import static com.brfpfa.Constants.*;

/**
 * This application takes a Date as Input and gives fascinating statistics and interesting facts about that date.
 */
public class Main {

    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.println("--** New Date **--");
            System.out.print("Introduceti ziua: ");
            int day = readInt();
            System.out.print("Introduceti Luna: ");
            int month = readInt();
            System.out.print("Introduceti Anul: ");
            int year = readInt();

            int days = countTheNumberOfDaysFromYearOne(day, month, year);

            System.out.println("1 Ianuarie " + year + " a fost o zi de: " + DAYS_OF_THE_WEEK[dayOfTheWeek(days)] + "\n");
        }

    }

    /**
     * @return the number of days that have passed from 1 January 0001 until the given date.
     */
    private static int countTheNumberOfDaysFromYearOne(int day, int month, int year) {
        int days = 0;

        // count year days
        days += countDaysOfTheYear(year);

        // count month days
        days += countDaysOfTheMonth(month, year);

        // add the days
        days += day - 1;
        return days;
    }

    /**
     * Returns the number of days from 1 January until the month given as
     * parameter.
     */
    private static int countDaysOfTheMonth(int month, int year) {
        int days = 0;
        for (int i = 1; i < month; i++) {
            days += DAYS_OF_THE_MONTH[i - 1];
        }
        if (month > 1 && bisect(year)) {// take into account that the current year might be bisect
            days++;
        }
        return days;
    }

    /**
     * Returns the number of days from 1 January 0001 until the year given as
     * parameter.
     */
    private static int countDaysOfTheYear(int year) {
        int days = 0;
        for (int i = 1; i < year; i++) {
            if (bisect(i)) {
                days += DAYS_OF_BISECT_YEAR;
            } else {
                days += DAYS_OF_NON_BISECT_YEAR;
            }
        }
        return days;
    }

    /**
     * Takes as a parameter a number of days and adds it to 1 January 0001.
     * Returns the day of the week of the computed day.
     *
     * @param days - number of days from day one
     * @return - the day of the week after days has passed.
     */
    private static int dayOfTheWeek(int days) {
        return days % 7;
    }

    private static boolean bisect(int year) {
        if (year % 400 == 0) {
            return true;
        }
        if (year % 100 == 0) {
            return false;
        }
        if (year % 4 == 0) {
            return true;
        }
        return false;
    }

    /**
     * Read an integer from keyboard.
     */
    private static int readInt() {
        int num = scan.nextInt();
        return num;
    }

}
