package com.brfpfa;

public class Constants {

	public static final String[] DAYS_OF_THE_WEEK = { "LUNI", "MARTI", "MIERCURI", "JOI", "VINERI", "SAMBATA",
			"DUMINICA" };

	public static final short DAYS_OF_BISECT_YEAR = 366;

	public static final short DAYS_OF_NON_BISECT_YEAR = 365;

	public static final short[] DAYS_OF_THE_MONTH = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

}